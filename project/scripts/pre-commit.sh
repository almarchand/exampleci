echo "Running pre-commit hook"

echo "Running unittests"
./project/scripts/run-unittest.sh

if [ $? -ne 0 ]; then
    echo "Unit tests must pass before commit"
    exit 1
fi

echo "unittests succeded"

echo "Running deployment tests"
./project/scripts/run-deploymenttests.sh

if [ $? -ne 0 ]; then
    echo "Deployments tests must pass before commit"
    exit 1
fi
